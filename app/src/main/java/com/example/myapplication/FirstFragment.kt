package com.example.myapplication

import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import androidx.navigation.fragment.findNavController
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    lateinit var mSignatureView: ElectronicSignatureView

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        val view = inflater.inflate(R.layout.fragment_first, container, false)
        mSignatureView = ElectronicSignatureView(context)
        mSignatureView.setWatermark(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date()) + " 222222")
        mSignatureView.sampleRate = 5
        mSignatureView.setBitmap(Rect(0, 0, 474, 158), 10, Color.WHITE)

        view.findViewById<RelativeLayout>(R.id.writeUserNameSpace).addView(mSignatureView)



        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_clear).setOnClickListener {
            mSignatureView.clear()
        }

        view.findViewById<Button>(R.id.button_save).setOnClickListener {
            if(mSignatureView.touched) { //SI FUE EDITADA
                val bitmap = mSignatureView.save(true, 0) // aqui se obtiene el bitmap de la firma
            }
        }
    }
}